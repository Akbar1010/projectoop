package org.example;

public class Husky extends Dog{
    public Husky(String name, int age) {
        super(name, age);
        this.eyeColor = EyeColor.Blue;
    }
}
