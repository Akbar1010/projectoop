package org.example;

public class Dog extends BaseData {
    public Dog(String name, int age) {
        super(name, age);
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public EyeColor getEyeColor() {
        return this.eyeColor;
    }

    public void run() {
        System.out.println("i'm running");
    }

    public void bark() {
        System.out.println("hawhaw");
    }

    public void play() {
        System.out.println("i'm playing");
    }

    @Override
    public String toString() {
        return "name: " + this.name + " age: " + this.age + " eye color: " + eyeColor;
    }
}
