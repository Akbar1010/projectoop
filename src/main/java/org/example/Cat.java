package org.example;

public class Cat extends BaseData {
    public Cat(String name, int age) {
        super(name, age);
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public EyeColor getEyeColor() {
        return this.eyeColor;
    }

    public void miyaw() {
        System.out.println("miyawmiyaw");
    }

    public void eat() {
        System.out.println("i'm eating");
    }

    public void jump() {
        System.out.println("i;m jumping");
    }

    public String toString() {
        return "name: " + this.name + " age: " + this.age + " eye color: " + eyeColor;
    }
}
