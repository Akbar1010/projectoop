package org.example;

public class Ovcharka extends Dog {
    public Ovcharka(String name, int age) {
        super(name, age);
        this.eyeColor = EyeColor.Black;
    }
}
