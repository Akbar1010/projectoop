package org.example;

public class Main {
    public static void main(String[] args) {
        Dog dog = new Husky("Tom", 5);
        System.out.println(dog);
        System.out.println(dog.getEyeColor());
        System.out.println(dog.getName());
        System.out.println(dog.getAge());
        Dog dog1 = new Ovcharka("Toplan", 4);
        System.out.println(dog1);
        System.out.println(dog1.getEyeColor());
        System.out.println(dog1.getName());
        System.out.println(dog1.getAge());
        Cat cat = new Misir("Bradley",2);
        System.out.println(cat);
        System.out.println(cat.getEyeColor());
        System.out.println(cat.getName());
        System.out.println(cat.getAge());
        Cat cat1 = new Britain("Allen",3);
        System.out.println(cat1);
        System.out.println(cat1.getEyeColor());
        System.out.println(cat1.getName());
        System.out.println(cat1.getAge());
    }
}