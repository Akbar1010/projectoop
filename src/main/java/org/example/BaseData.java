package org.example;

public class BaseData {
    public String name;
    public int age;
    EyeColor eyeColor;

    public BaseData(String name, int age) {
        this.name = name;
        this.age = age;
    }
}
enum EyeColor {
    Green,
    Black,
    Purple,
    Blue
}
