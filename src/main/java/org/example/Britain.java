package org.example;

public class Britain extends Cat {
    public Britain(String name, int age) {
        super(name, age);
        this.eyeColor = EyeColor.Purple;
    }
}
